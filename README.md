
This is a React project bootstrapped with [`Create React App`](https://create-react-app.dev/) using the default Typescript template. 

[`Material UI`](https://mui.com/material-ui/getting-started/overview/) is used as styling/UI-components library.

### Getting Started

```bash
npm install && npm start
```

### Basic requirements

- Your preferred IDE / Code Editor
- NodeJS > 12
- Your preferred browser (tested on Chrome and Firefox)

### Folder Structure
    .
    ├── ...
    ├── src                  
    │   ├── components       # UI components following Atomic Design standards
    │   ├── contexts         # React context's and providers
    │   └── hooks            # Common application custom hooks
    │   └── lib              # API Clients, models, constants, helpers...
    └── ...
    
### Linting and formatting

The linting and formatting is done using eslint and prettier, configured using common industry standards.

Exercise

● Infinite scroll showing employees list with their contact information.
The information is mocked at runtime while a REST API is being built to assemble on the
application front end.
Here is a list of the features that you should add to the application:
● Fix an existing bug on the infinite-scroll pagination.
● Edit existing contacts information — changes must be visible on the person’s details
card and the main page list.
● Store the previous changes across page refreshes.

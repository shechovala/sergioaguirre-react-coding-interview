import { useContactList } from "@hooks/contacts/useContactList";
import { IPerson } from "@lib/models/person";
import { Avatar, Box, Button, Card, Input } from "@mui/material"
import { useEffect, useState } from "react";
import { Form, useParams } from "react-router-dom";

export interface IContactDetailProps { person?: IPerson; }

function ContactDetail({ person: { id, firstName, lastName, email }, }: IContactDetailProps) {
  if (!id) return null;

  return (
    <Card sx={{ mt: 1.5, p: 4 }}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar sx={{ mr: 1.5 }} />
        <p><span>id</span>{id}<span></span></p>
      </Box>
      <Form>
        <Box display="flex" flexDirection="column" alignItems="center">
          <Input name="name" value={firstName} />
          <Input name="lastName" value={lastName} />
          <Input name="email" value={email} />
          <Button onClick={e => console.log(e)} sx={{ mt: 1 }} variant="contained" color="primary">
            Save
          </Button>
        </Box>
      </Form>
    </Card>
  );
}

const ContactDetailPage = () => {
  const { id } = useParams();
  const { loading, fetchContact } = useContactList()

  const [person, setPerson] = useState<IPerson>()


  useEffect(() => {
    const fetch = async () => {
      const person = await fetchContact(id)
      setPerson(person)
    }
    fetch()
  }, [id])

  if (loading) {
    return (
      <div>
        <p>Loading...</p>
      </div>
    )
  }
  return (
    <ContactDetail person={person} />
  )
}

export default ContactDetailPage
